
$(function () {

    //console.log("jquery is loaded");
  
    initSeating();

})

function initSeating() {

    let maxSeats = 3;

    $(".seat").on('click',  function () {
        console.log("You clicked a seat");

        if( $(this).hasClass("reserved")) return;

        let currentCount = $(".selected").length;
     
        if (currentCount < maxSeats && $(this).hasClass("selected")) {

            $(this).removeClass("selected");
            return;
        }

        if (currentCount < maxSeats) {

            $(this).addClass("selected");

        } else if ( $(this).hasClass("selected")) {

            $(this).removeClass("selected");
        }

        let seatId = $(this).data("seatid");
        console.log(seatId);

        let msg = `You chose seat id ${seatId}`;

        $('#bookingInfo').html(msg);

    })

}



