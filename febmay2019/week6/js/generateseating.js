let cinema = [

    {title: "Screen 1", rows: 10, cols:10 , reserved:[4,5,6]},
    {title: "Screen 2", rows: 20, cols:10, reserved:[4,5,6]},
    {title: "Screen 3", rows: 5, cols:10, reserved:[4,5,6] },
    {title: "Screen 4", rows: 40, cols:18 , reserved:[4,5,6]}



]



$(function () {

    let currentScreen = cinema[2];
    renderSeating(currentScreen);

})

function renderSeating(screen) {

    rows = screen.rows;
    cols = screen.cols;
    let templateData = [];

    let count = 1;
  for(i = 0; i < rows; i++) {

    for(j = 0; j < cols; j++, count++) {

      

        if ( j == 0) {
                 templateData.push( getSeatTemplate(i, j, count,'clear') )
        } else {

            templateData.push( getSeatTemplate(i, j, count,'') )
        }


    }

   



  }


  $('#movieSeating').empty().append( templateData.join(" "));

  for(i = 0; i < screen.reserved.length; i++) {
    let currentReservedId = screen.reserved[i];
    $(`#seat_${currentReservedId}`).addClass("reserved");

}

}

function getSeatTemplate(row, col, id, clear) {
    return  `<div id='seat_${id}' class="seat ${clear}" data-row="${row}" data-col="${col}" data-seatid="${id}">${row},${col}</div>`


}